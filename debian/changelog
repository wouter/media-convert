libmedia-convert-perl (1.4.1-1) unstable; urgency=medium

  * New upstream version: use channelmap filter instead of channelsplit
    one for extracting a single channel.

 -- Wouter Verhelst <wouter@debian.org>  Sun, 22 Dec 2024 14:18:52 +0200

libmedia-convert-perl (1.4.0-1) unstable; urgency=medium

  * New upstream version, yet again.

 -- Wouter Verhelst <wouter@debian.org>  Thu, 12 Dec 2024 18:41:26 +0200

libmedia-convert-perl (1.3.0-1) unstable; urgency=medium

  * New upstream release with more ffmpeg 7 fixes

 -- Wouter Verhelst <wouter@debian.org>  Mon, 21 Oct 2024 09:04:45 +0200

libmedia-convert-perl (1.2.0-1) unstable; urgency=medium

  * New upstream release.
    - Fixes behavior with ffmpeg 7; Closes: #1072339.

 -- Wouter Verhelst <wouter@debian.org>  Mon, 19 Aug 2024 10:21:48 +0200

libmedia-convert-perl (1.1.0-2) unstable; urgency=medium

  * Add support for input_parameters

 -- Wouter Verhelst <wouter@debian.org>  Fri, 15 Sep 2023 14:51:49 +0530

libmedia-convert-perl (1.1.0-1) unstable; urgency=medium

  * New upstream release
    - With support for converting synfig animations to video

 -- Wouter Verhelst <wouter@debian.org>  Sat, 02 Sep 2023 10:17:29 +0200

libmedia-convert-perl (1.0.5-3) unstable; urgency=medium

  * Fix up test suite for ffmpeg 6.0. Closes: #1042431, for reals this
    time.

 -- Wouter Verhelst <wouter@debian.org>  Sat, 29 Jul 2023 19:08:21 +0200

libmedia-convert-perl (1.0.5-2) unstable; urgency=medium

  * debian/control: add missing libsemver-perl dependency.
    Closes: #1042431.
  * Media::Convert::Asset: revert accidental removal of
    audio_channel_count
  * Media::Convert::Asset::av1: sync docs with reality

 -- Wouter Verhelst <wouter@debian.org>  Thu, 27 Jul 2023 09:29:39 +0200

libmedia-convert-perl (1.0.5-1) unstable; urgency=medium

  * Media::Convert::Asset::Profile::av1: default to using preset 4
    rather than 2, disable video bit rates, and deafult to using vorbis
    rather than opus for audio (because the former is more flexible).

 -- Wouter Verhelst <wouter@debian.org>  Wed, 26 Jul 2023 11:27:41 +0200

libmedia-convert-perl (1.0.4-3) unstable; urgency=medium

  * Revert the extra -map parameter. It does not have the effect we
    thought it did.

 -- Wouter Verhelst <wouter@debian.org>  Tue, 07 Mar 2023 19:54:35 +0200

libmedia-convert-perl (1.0.4-2) unstable; urgency=medium

  * Media::Convert::Map: add explicit -map parameter for the audio
    channel if we also specify a -map_channel one.

 -- Wouter Verhelst <wouter@debian.org>  Sat, 04 Mar 2023 14:47:20 +0200

libmedia-convert-perl (1.0.4-1) unstable; urgency=medium

  * New upstream release

 -- Wouter Verhelst <wouter@debian.org>  Wed, 15 Feb 2023 10:29:05 +0200

libmedia-convert-perl (1.0.3-2) unstable; urgency=medium

  * Skip av1 tests unconditionally

 -- Wouter Verhelst <wouter@debian.org>  Thu, 10 Nov 2022 22:35:43 +0200

libmedia-convert-perl (1.0.3-1) unstable; urgency=medium

  * New upstream release, with fixes to mc-encode script

 -- Wouter Verhelst <wouter@debian.org>  Wed, 02 Nov 2022 09:25:45 +0200

libmedia-convert-perl (1.0.2-4) unstable; urgency=medium

  * debian/tests/pkg-perl/smoke-setup: skip av1 test on 32-bit arm
    architectures as a workaround for #1022870 in src:svt-av1

 -- Wouter Verhelst <wouter@debian.org>  Tue, 01 Nov 2022 16:19:36 +0200

libmedia-convert-perl (1.0.2-3) unstable; urgency=medium

  * Add autopkgtest
  * Ran wrap-and-source -bastk
  * debian/control: add missing dep on libmoose-perl and libmoosex-singleton-perl

 -- Wouter Verhelst <wouter@debian.org>  Wed, 26 Oct 2022 13:39:16 +0200

libmedia-convert-perl (1.0.2-2) unstable; urgency=medium

  * debian/control: fix typo

 -- Wouter Verhelst <wouter@debian.org>  Wed, 26 Oct 2022 11:40:25 +0200

libmedia-convert-perl (1.0.2-1) unstable; urgency=medium

  * Convert to non-native package (should never have been native in the first
    place)

 -- Wouter Verhelst <wouter@debian.org>  Mon, 24 Oct 2022 12:27:12 +0200

libmedia-convert-perl (1.0.2) unstable; urgency=medium

  * Ensure the av1 codec is supported at all before we try checking
    which encoder provides it, to avoid an undefined error.
  * mc-encode: fix use of ProfileFactory. It's
    Media::Convert::Asset::ProfileFactory, not
    Media::Convert::ProfileFactory.
  * Add libjson-maybexs-perl also to Depends (not just Build-Depends)

 -- Wouter Verhelst <wouter@debian.org>  Fri, 21 Oct 2022 08:57:45 +0200

libmedia-convert-perl (1.0.1) unstable; urgency=medium

  * Initial release.

 -- Wouter Verhelst <wouter@debian.org>  Tue, 06 Sep 2022 16:56:11 +0200
